---
id: basic
title: Basic Markdown Features
slug: /
sidebar_position: 1
---

# Heading{#top}

     Heading are automatically shown on the right sidebar. Id can be written in \{#name} for headings and be connected with link

## Sub heading 1 ##2 hash tag

    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

### Small heading ###3 hash tag

    h1 , h2 ,h3 can be represented with number of # up to h5

## Code snippets

    You can write code by wrapping in  \``` and \```. You can choose the language after the first 3 `. Docusaurus automatically let you copy the code.

    ```javascript
        console.log('Arpalar Java to the script')
    ```

## Bold and italic

you can make something _italic_  
you can make something **bold**  
you can make something **_bold and italic_**

## Order and Unordered Lists

1. List starts
1. ordered lists
1. lists end

- List starts
- ordered lists
- lists end

## Links

**links** can be written like [this](#top) \[name](link)  
[back to top](#top)
